var TokenHelper = {
    getParameter: function (sParam, sPageURL) {
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    },
    getHashParameter: function (sParam) {
        return this.getParameter(sParam, window.location.hash.substring(1));
    },
    getUrlParametr: function (sParam) {
        return this.getParameter(sParam, window.location.search.substring(1));
    },
    getToken: function () {
        return this.getHashParameter('access_token');
    },
    handleSaveToken: function () {
        var access_token = this.getToken();
        if (!!access_token) {
            console.log('try save token');
            this.saveToken(access_token)
        } else{
            console.log('token not present. pass');
        }
    },
    saveToken: function (access_token) {
        $.ajax('/vk_token', {
            data: {
                access_token: access_token
            },
            success: function () {
                console.log('token saved');
                window.location.replace('/');
            },
            error: function () {
                console.log('error. token not saved.');
                window.location.replace('/');
            }
        })
    }
};


$(function() {
   TokenHelper.handleSaveToken();
});