var TokenHelper = {
    getParameter: function (sParam, sPageURL) {
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    },
    getHashParameter: function (sParam) {
        return this.getParameter(sParam, window.location.hash.substring(1));
    },
    getUrlParametr: function (sParam) {
        return this.getParameter(sParam, window.location.search.substring(1));
    },
    getToken: function () {
        return this.getHashParameter('access_token');
    },
    handleSaveToken: function () {
        var access_token = this.getToken();
        if (!!access_token) {
            console.log('try save token');
            this.saveToken(access_token)
        } else {
            console.log('token not present. pass');
        }
    },
    saveToken: function (access_token) {
        $.ajax('/vk_token', {
            data: {
                access_token: access_token
            },
            success: function () {
                console.log('token saved');
                window.location.replace('/');
            },
            error: function () {
                console.log('error. token not saved.');
                window.location.replace('/');
            }
        })
    }
};


var SocketHelper = {
    socket: undefined,
    messageListeners: [],
    ready: false,

    initSocket: function () {
        var socket = new WebSocket("ws://localhost:8082/socket");
        var self = this;
        socket.onopen = function () {
            console.log("Соединение установлено.");
            self.ready = true;
        };
        socket.onclose = function (event) {
            if (event.wasClean) {
                console.log('Соединение закрыто чисто');
            } else {
                console.log('Обрыв соединения'); // например, "убит" процесс сервера
            }
            console.log('Код: ' + event.code + ' причина: ' + event.reason);
        };
        socket.onmessage = function (event) {
            console.log('Получены данные ');
            var data = JSON.parse(event.data);
            console.log(data);
            self.messageListeners.map(function (listener) {
                return listener(data);
            })
        };
        socket.onerror = function (error) {
            console.log('Ошибка ');
            console.log(error.message);
        };

        this.socket = socket;
    },
    sendMessage: function (event, data) {
        var self = this;
        var send_func = function () {
            if (!self.ready) {
                alert('Socket timeout');
            } else {
                self.socket.send(JSON.stringify({'event': event, 'data': data}));
            }
        };
        if (!this.ready) {
            setTimeout(send_func, 2000);
        } else {
            send_func();
        }
    },
    addMessageListener: function (listener) {
        console.log('added listener');
        this.messageListeners.push(listener);
    }
};
SocketHelper.initSocket();
SocketHelper.sendMessage('init_audios', true);


angular.module('Audio', [])
    .controller('AudioCtrl', function ($scope, $sce) {
        $scope.trustSrc = function (src) {
            return $sce.trustAsResourceUrl(src);
        };
        $scope.visible = false;
        $scope.downloaded = undefined;
        $scope.audios = {
        };
        $scope.saveAudio = function (audio_data) {
            var search_str = audio_data['search_str'];
            var audio = audio_data['audio'];
            if (!!audio) {
                $scope.audios[search_str] = {
                    name: audio['title'],
                    src: audio['url'],
                    artist: audio['artist'],
                    searchStage: !!audio['searchStage'] ? audio['searchStage'] : 0,
                    path: !!audio['path'] ? audio['path'] : ''
                };
                $scope.visible = true;


                $scope.$apply();
            }
        };

        Array.prototype.chooseRandom = function () {
            return this[Math.floor(Math.random() * (this.length + 1)) ];
        };
        function sec() {

            var a = ['Taylor Swift Shake It Off', 'Animals Maroon 5', 'Dont Ed Sheeran', 'Trumpets Jason Derulo', 'Григорий Лепс Лодон'];
            $scope.holder = 'Пример: ' + a.chooseRandom();

        }

        setInterval(sec, 1000);

        $scope.saveAll = function (audio_data) {

            var progress = audio_data['progress'];
            if (progress == -1) {
                $scope.audioname = "все сохранено"
            } else {
                $scope.audioname = audio_data['audio'] + " сохраняется";
                $scope.total = progress;
            }

            $scope.$apply();

        };

        $scope.handleAudioDownloaded= function(audio_data){
            var downloaded_key = audio_data['search_str'];
            if(!!$scope.audios[downloaded_key]){
                $scope.downloaded = $scope.audios[downloaded_key];
            }
            $scope.$apply();
        };

        SocketHelper.addMessageListener(function (message) {
            if (message.event == 'save_audio') {
                if (!!message.data) {
                    $scope.saveAudio(message.data);
                } else {
                    alert('Песня не найдена')
                }
            }

            if (message.event == 'audio_downloaded') {
                if (!!message.data) {
                    console.log('call handleAudioDownloaded');
                    $scope.handleAudioDownloaded(message.data);
                } else {
                    alert('Песня не найдена')
                }
            }
        });

        $scope.searchAudio = function (data) {
            var search_str = $scope.audioName;
            $scope.audioName = '';
            if (!!search_str) {
                SocketHelper.sendMessage('save_audio', search_str);
            }
        };

        $scope.removeAudio = function (key) {
            var audio = $scope.audios[key];
            if (!!audio) {
                delete $scope.audios[key];
                SocketHelper.sendMessage('delete_audio', key);
            }
        };

        $scope.audiosLength = function () {
            return Object.keys($scope.audios).length;
        };

        $scope.download_all = function () {
            SocketHelper.sendMessage('download_all');
        };

        $scope.sync_vk = function () {
            SocketHelper.sendMessage('sync_vk');
        };

        $scope.remaining = function () {
            var count = 0;

            angular.forEach($scope.audios, function (audio) {
                count += audio.searchStage == 100 ? 0 : 1;
            });
            return count;
        };

        $scope.downloaded_audios = function () {
            var count = 0;

            angular.forEach($scope.audios, function (audio) {
                count += !!audio.path ? 1 : 0;
            });
            return count;
        };

        $scope.downloaded_name = function(){
            if(!!$scope.downloaded){
                return $scope.downloaded['name'];
            }  else{
                return '';
            }
        };

        $scope.download_state = function () {
            var downloaded = $scope.downloaded_audios();
            var audiosLength = $scope.audiosLength();
            if (downloaded == audiosLength) {
                return 100;
            } else {
                return (downloaded/ audiosLength)*100 ;
            }
        }


    })


    .directive('ngPlaceholder', function ($document) {
        return {
            restrict: 'A',
            scope: {
                placeholder: '=ngPlaceholder'
            },
            link: function (scope, elem, attr) {
                scope.$watch('placeholder', function () {
                    elem[0].placeholder = scope.placeholder;
                });
            }
        }
    })
;


$(function () {
    TokenHelper.handleSaveToken();
    $('.js-get_token-vk').on('switchChange.bootstrapSwitch', function () {
        var elem = $(this);
        if (elem.is(':checked')) {
            window.location.replace(elem.data('token_url'))
        }
    });
});