# coding=utf-8
"""
Главный файл
"""
import os
import webbrowser
from app.configs import VK_TOKEN_FILE
import eventlet
from eventlet import wsgi
from app.views import uri_handler

if __name__ == "__main__":
    eventlet.monkey_patch()
    if os.path.exists(VK_TOKEN_FILE):
        os.remove(VK_TOKEN_FILE)  # токен устаревает, лучше его пока не сохранять между запусками
    site_listener = eventlet.listen(('127.0.0.1', 8082))
    webbrowser.open("http://localhost:8082/")
    wsgi.server(site_listener, uri_handler)
