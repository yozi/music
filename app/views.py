# coding=utf-8
"""
Представления
"""
import mimetypes
from app.parsers.vk import save_access_token
from app.socket import socket_handler


mimetypes.init()

import os
from app.configs import ROOT_DIRECTORY, ENCODING
from app.parsers import vk
from jinja2 import Environment, FileSystemLoader
from urllib.parse import parse_qs

jinja_env = Environment(loader=FileSystemLoader(os.path.join(ROOT_DIRECTORY, 'templates')))


def index():
    template = jinja_env.get_template('index.html')
    context = {'title': 'Главная страница', 'vk_auth_url': vk.get_auth_url('localhost:8082?vk_token=1'),
               'is_set_token': bool(vk.get_access_token())}
    return template.render(context)


def vk_token(access_token):
    with open(os.path.join(ROOT_DIRECTORY, 'data', 'vk_token.data'), 'w') as f:
        f.write(access_token)
    return index()


def static(file_path):
    """
    Раздача статики: js, css, images, etc. Уязвимости осознано не закрыты
    """

    with open(file_path, 'rb') as f:
        data = f.read()

    return data


def error404():
    template = jinja_env.get_template('404.html')
    context = {'title': 'Страница не найдена'}
    return template.render(context)


def uri_handler(env, start_response):
    if env['PATH_INFO'] == '/':
        body = index()
        response_headers = [
            ('Content-type', 'text/html'),
        ]
        start_response('200 OK', response_headers)
    elif env['PATH_INFO'].startswith('/static/') and os.path.exists(os.path.join(ROOT_DIRECTORY, env['PATH_INFO'][1:])):
        file_path = os.path.join(ROOT_DIRECTORY, env['PATH_INFO'][1:])
        file_data = static(file_path)
        mime_type = mimetypes.types_map.get(os.path.splitext(file_path)[1], 'text/plain')
        response_headers = [
            ('Content-type', mime_type),
        ]
        start_response('200 OK', response_headers)

        return [file_data]
    elif env['PATH_INFO'] == '/socket':
        return socket_handler(env, start_response)
    elif env['PATH_INFO'] == '/vk_token':
        d = parse_qs(env['QUERY_STRING'])

        # In this idiom you must issue a list containing a default value.
        try:
            access_token = d.get('access_token', [])[0]
            save_access_token(access_token)
        except IndexError:
            pass
        body = index()
        response_headers = [
            ('Content-type', 'text/html'),
        ]
        start_response('200 OK', response_headers)
    else:
        body = error404()
        response_headers = [
            ('Content-type', 'text/html'),
        ]
        start_response('404 Not Found', response_headers)
    return [bytes(body, encoding=ENCODING)]