# coding=utf-8
"""
Файлы конфигурации
"""
# при сшивании в один архив путь "минус 2 уровня",  как сайт "минус уровень"  Пока не разобрался с setup.py полностью
import os

ENCODING = 'utf8'
ROOT_DIRECTORY = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
DOWNLOAD_DIR = os.path.join(ROOT_DIRECTORY, 'data')
PARSERS = {
    'vk': {
        'id': 4626032,
    }
}
VK_TOKEN_FILE = os.path.join(ROOT_DIRECTORY, 'data', 'vk_token.data')
