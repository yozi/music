# coding=utf-8
"""
Парсер музыки из контакта
"""
import json
import os
from urllib import parse, error
from app.configs import PARSERS, VK_TOKEN_FILE, DOWNLOAD_DIR
import eventlet
from eventlet.green import urllib

app_id = PARSERS.get('vk', {}).get('id', 0)
vk_poll = eventlet.GreenPool(5)
vk_download_poll = eventlet.GreenPool(10)


def get_auth_url(redirect_url):
    params = {'client_id': app_id, 'scope': 'audio', 'redirect_uri': redirect_url, 'display': 'page',
              'response_type': 'token', 'v': '5.26'}
    params = parse.urlencode(params)
    url = '{url}?{params}'.format(url='https://oauth.vk.com/authorize', params=params)

    return url


def save_access_token(access_token):
    with open(VK_TOKEN_FILE, 'w') as f:
        f.write(access_token)


def get_access_token():
    access_token = None
    if os.path.exists(VK_TOKEN_FILE):
        with open(VK_TOKEN_FILE, 'r') as f:
            access_token = f.read()

    return access_token


class VkParser(object):
    """
    :type audio_url: str
    :type api_id: int
    :type token: str
    :type audios: dict [str, VkAudioSet]
    """
    search_url = 'https://api.vk.com/method/audio.search'
    get_url = 'https://api.vk.com/method/audio.get'

    def __init__(self, api_id=None):
        super().__init__()
        self.api_id = api_id
        self.access_token = get_access_token
        self.audios = dict()

    def search_audios(self, search_str):
        params = {'q': search_str, 'access_token': self.access_token(), 'count': 30, 'v': '5.26'}
        params = parse.urlencode(params)
        url = '{url}?{params}'.format(url=self.search_url, params=params)
        req = urllib.Request(url=url, headers={'Accept': 'application/json'})
        try:
            response = urllib.urlopen(req)
            data = response.read().decode('utf8', 'ignore')
        except error.HTTPError as e:
            return []

        audios_data = json.loads(data).get('response', {}).get('items', [])

        return audios_data

    def add_audio_set(self, search_str):
        audios_data = self.search_audios(search_str)
        if audios_data:
            audio_set = VkAudioSet(search_str, audios_data)
            self.audios[search_str] = audio_set
            return audio_set
        else:
            return None

    def call_downloads(self, pre_hook=None, after_hook=None):
        """
        :param pre_hook: Функция, вызываемая перед скачиванием главной песни из каждого vkAudioSet
            В неё передается 1 параметр:  VkAudioSet объект
        :param after_hook: Функция, вызываемая по завершению скачивания главной песни из каждого vkAudioSet
            В неё передаются 2 параметра:  VkAudioSet объект и bool факт успеха скачивания его текущей главной песни
        """

        def download(audio_set_):
            result_ = False
            if audio_set_.get_search_percent() == 100:
                if callable(pre_hook):
                    print('call pre_hook')
                    pre_hook(audio_set_)
                print('in download')
                result_ = audio_set_.main_audio.download_self()
                if callable(after_hook):
                    print('call after_hook')
                    after_hook(audio_set_, result_)
                print('result download ', audio_set_.main_audio.path, ' for ', result_)
            return result_

        audio_values = tuple(self.audios.values())  # количество значений не должно меняться в цикле

        for result in vk_download_poll.imap(download, audio_values):
            pass

    def sync_vk(self, hook):
        """
        :param hook: функция, вызываемая для каждой песни, полученной от контакта. Её параметр - VkAudioSet
        """
        print('in sync_vk')
        audios_data = self.get_user_audios()

        def search(audio_data):
            """
            :type audio_data: dict
            """
            print('in sync_vk search')
            audio_set_ = self.add_audio_set(
                '{artist} {title}'.format(artist=audio_data.get('artist', ''), title=audio_data.get('title', '')))
            if audio_set_ and callable(hook):
                print('call hook')
                hook(audio_set_)
                result_ = True
            else:
                result_ = False

            eventlet.sleep(1)  # попытка не ссориться с защитой ВК. Потоков 5 => задержка между скачиваниями ~0.2
            print('search success = ', result_)
            if not result_:
                eventlet.sleep(10)  # если False - почти наверняка контакт блокирует за частые запросы, притормозить
            return result_

        for result in vk_poll.imap(search, audios_data):
            pass

    def get_user_audios(self):
        """
        Возвращает список аудиозаписей пользователя, максимум 6000
        """
        params = {'count': 6000, 'v': '5.26',  'access_token': self.access_token()}
        params = parse.urlencode(params)
        url = '{url}?{params}'.format(url=self.get_url, params=params)
        req = urllib.Request(url=url, headers={'Accept': 'application/json'})
        try:
            response = urllib.urlopen(req)
            data = response.read().decode('utf8', 'ignore')
        except error.HTTPError as e:
            return []

        audios_data = json.loads(data).get('response', {}).get('items', [])

        return audios_data


class VkAudioSet(object):
    """
    :type audios: list of VkAudio
    :type search_str: str
    :type main_audio: VkAudio
    """
    handled_bitrates = 0
    main_audio = None

    def __init__(self, search_str, audios_data):
        if not audios_data:
            raise Exception("Can't create empty AudioSet")
        super().__init__()
        self.search_str = search_str
        self.audios = [VkAudio(audio) for audio in audios_data]
        self.main_audio = self.get_best_audio()

    def get_best_audio(self):
        return sorted(self.audios, key=lambda audio: audio.bitrate if audio.bitrate else 0, reverse=True)[0]

    def update_main_audio(self):
        self.main_audio = self.get_best_audio()

    def add_bitrates(self, hook=None):
        """
        :param hook: Функция, вызываемая при обработке каждой песни с 2 аргументами:
            VkAudioSet объект и bool факт успеха определения битрейта очередной песни этого объекта
        """

        def fetch(vk_audio):
            print('in fetch')
            result_ = vk_audio.add_bitrate()
            self.handled_bitrates += 1
            eventlet.sleep(1)  # попытка не ссориться с защитой ВК. Потоков 5 => задержка между скачиваниями ~0.2
            return result_

        for result in vk_poll.imap(fetch, self.audios):
            print("result ", result)
            if callable(hook):
                hook(self, result)

    def get_search_percent(self):
        return int(self.handled_bitrates * 100 / len(self.audios)) if self.handled_bitrates < len(self.audios) else 100


class VkAudio(object):
    def __init__(self, audio_data):
        super().__init__()
        self.aid = audio_data.get('id', 0)
        self.artist = audio_data.get('artist', 'Unknown artist')
        self.duration = audio_data.get('duration', 0)
        self.genre = audio_data.get('genre', 18)
        self.lyrics_id = audio_data.get('lyrics_id', None)
        self.owner_id = audio_data.get('owner_id', 0)
        self.title = audio_data.get('title', 'Unknown title')
        self.url = audio_data.get('url', None)
        self.file_size = None
        self.bitrate = None
        self.path = None

    def download_self(self):
        if self.path is None:
            try:
                self.path, headers = urllib.urlretrieve(self.url, os.path.join(DOWNLOAD_DIR,
                                                                               "{artist} - {name}.mp3".format(
                                                                                   name=self.title,
                                                                                   artist=self.artist)))
            except error.HTTPError as e:
                return False

        return True

    def add_bitrate(self):
        req = urllib.Request(url=self.url, headers={'Accept': 'application/json'}, method='HEAD')
        try:
            response = urllib.urlopen(req)
            meta = response.info()
            headers = dict(meta._headers)
            self.file_size = int(headers.get('Content-Length', 0))
            self.bitrate = int(self.file_size * 8 / self.duration / 1024)
        except error.HTTPError as e:
            return False
        except IndexError as e:
            return False

        return True

    def to_dict(self):
        return {
            'id': self.aid,
            'artist': self.artist,
            'duration': self.duration,
            'genre': self.genre,
            'lyrics_id': self.lyrics_id,
            'owner_id': self.owner_id,
            'title': self.title,
            'url': self.url,
            'file_size': self.file_size,
            'bitrate': self.bitrate,
            'path': self.path,
        }


vk_parser = VkParser()


