# coding=utf-8
"""
Обработка событий сокета
"""
import json
from app.parsers.vk import vk_parser
from eventlet import websocket
import eventlet

socket_poll = eventlet.GreenPool(10)


class SocketHelper(object):
    def __init__(self, ws):
        self.ws = ws
        self.audio_parser = vk_parser

    def handle_input(self, message_raw):
        message = json.loads(message_raw)
        event = message.get('event', None)
        data = message.get('data', None)
        if event == 'save_audio':
            print('try add audio')
            self.send_message('debug', data)
            audio_set = self.audio_parser.add_audio_set(data)
            response_data = {'search_str': data, 'audio': audio_set.main_audio.to_dict()} if audio_set else None
            self.send_message('save_audio', response_data)

            if audio_set:
                audio_set.add_bitrates(lambda vk_set, result: self.audio_save_message(vk_set))
                audio_set.update_main_audio()
                self.audio_save_message(audio_set)
        elif event == 'download_all':
            print('CALLED download_all')

            def message_downloaded(vk_set, result):
                if result:
                    self.audio_save_message(vk_set)
                    response_data_ = {'search_str': vk_set.search_str}
                    self.send_message('audio_downloaded', response_data_)

            self.audio_parser.call_downloads(
                after_hook=message_downloaded
            )
        elif event == 'delete_audio':
            key = message['data']
            if key in self.audio_parser.audios:
                del self.audio_parser.audios[key]
            self.send_message('delete_audio', True)
        elif event == 'init_audios':
            print('init_audios')
            for search_str in self.audio_parser.audios:
                self.audio_save_message(self.audio_parser.audios[search_str])

        elif event == 'sync_vk':
            print('sync_vk')

            def hook(vk_set):
                self.audio_save_message(vk_set)
                vk_set.add_bitrates(lambda vk_set_, result: self.audio_save_message(vk_set_))

            self.audio_parser.sync_vk(hook)
        else:
            print('unknown event ', event)

    def audio_try_download_message(self, audio_set):
        print('audio_try_download_message')
        response_data = {'search_str': audio_set.search_str}
        self.send_message('try_download_audio', response_data)

    def audio_save_message(self, audio_set):
        audio_data = audio_set.main_audio.to_dict()
        audio_data['searchStage'] = audio_set.get_search_percent()
        response_data = {'search_str': audio_set.search_str, 'audio': audio_data}
        self.send_message('save_audio', response_data)

    def send_message(self, event, data):
        response = json.dumps({'event': event, 'data': data})
        self.ws.send(response)


@websocket.WebSocketWSGI
def socket_handler(ws):
    helper = SocketHelper(ws)
    while True:
        message = ws.wait()
        if message is None:
            break
        else:
            socket_poll.spawn(helper.handle_input, message)
