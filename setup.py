# coding=utf-8
"""
Документация
"""
import os
import sys
from cx_Freeze import setup, Executable

ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, os.path.abspath(ROOT_PATH))

productName = "MusicSync"



def get_files(catalogs):
    find_files = []
    for catalog in catalogs:
        for root, dirs, files in os.walk(catalog):
            find_files += [os.path.join(root, name) for name in files]
    return find_files


options = {
    'build_exe': {
        'compressed': True,
        'includes': [
            'ftplib',
            'urllib',
            'urllib.request',
        ],
        'include_files': get_files(['templates', 'static', 'data']),
        'path': sys.path
    }
}

executables = [
    Executable('run.py'),
]

setup(name='MusicSync',
      version='0.1',
      description='Test build exe',
      options=options,
      executables=executables
)